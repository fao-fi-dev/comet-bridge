/**
 * (c) 2015 FAO / UN (project: comet-bridge-converter)
 */
package org.fao.fi.comet.bridge.converter;

import java.net.URISyntaxException;

import org.fao.fi.comet.bridge.mapping.model.CometMappingData;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingsData;
import org.fao.fi.comet.core.patterns.data.providers.DataProvider;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 30, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 30, 2015
 */
public class UniformCometFormatConverter<DATA> extends CometFormatConverter<DATA, DATA, MatchingEngineProcessConfiguration>{
	public CometMappingData convert(MatchingEngineProcessConfiguration config, 
							   DataProvider<DATA> sourceProvider, 
							   DataProvider<DATA> targetProvider, 
							   MatchingsData<DATA, DATA> mappings, 
							   Class<DATA> dataType, 
							   String identifier,
							   String version, 
							   String author, 
							   String description, 
							   boolean verbose) throws URISyntaxException {
		return super.convert(config, sourceProvider, targetProvider, mappings, dataType, dataType, identifier, version, author, description, verbose);
	}
}