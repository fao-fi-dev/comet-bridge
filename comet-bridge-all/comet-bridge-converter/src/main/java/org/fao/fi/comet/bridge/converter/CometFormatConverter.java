/**
 * (c) 2015 FAO / UN (project: comet-bridge-converter)
 */
package org.fao.fi.comet.bridge.converter;

import static org.fao.fi.comet.bridge.mapping.dsl.DataProviderDSL.provider;
import static org.fao.fi.comet.bridge.mapping.dsl.MappingContributionDSL.matchlet;
import static org.fao.fi.comet.bridge.mapping.dsl.MappingDSL.map;
import static org.fao.fi.comet.bridge.mapping.dsl.MappingDetailDSL.target;
import static org.fao.fi.comet.bridge.mapping.dsl.MappingElementDSL.wrap;
import static org.fao.fi.comet.bridge.mapping.dsl.MappingElementIdentifierDSL.identifierFor;
import static org.fao.fi.comet.bridge.mapping.model.utils.jaxb.JAXB2DOMUtils.asElement;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import org.fao.fi.comet.bridge.mapping.model.CometDataProvider;
import org.fao.fi.comet.bridge.mapping.model.CometMapping;
import org.fao.fi.comet.bridge.mapping.model.CometMappingData;
import org.fao.fi.comet.bridge.mapping.model.CometMappingDetail;
import org.fao.fi.comet.bridge.mapping.model.CometMappingScoreType;
import org.fao.fi.comet.bridge.mapping.model.CometMatchletConfiguration;
import org.fao.fi.comet.bridge.mapping.model.CometMatchletConfigurationProperty;
import org.fao.fi.comet.core.model.engine.Matching;
import org.fao.fi.comet.core.model.engine.MatchingDetails;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingResult;
import org.fao.fi.comet.core.model.engine.MatchingsData;
import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.model.matchlets.MatchletConfigurationParameter;
import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 29, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 29, 2015
 */
public class CometFormatConverter<SOURCE,
								  TARGET,
								  CONFIG extends MatchingEngineProcessConfiguration> {
	protected Logger _log = LoggerFactory.getLogger(this.getClass());
	


	public CometMappingData convert(MatchingEngineProcessConfiguration config,
									DataProvider<SOURCE> sourceProvider, 
									DataProvider<TARGET> targetProvider, 
									MatchingsData<SOURCE, TARGET> mappings, 
									Class<SOURCE> sourceType,
									Class<TARGET> targetType,
									String identifier,
									String version,
									String author,
									String description,
									boolean verbose) throws URISyntaxException {

		CometDataProvider source, target;
		
		source = provider("http://provider.comet.fi.fao.org/" + sourceProvider.getClass().getName()).of("http://data.comet.fi.fao.org/" + sourceType.getName()).named("urn:provider#source");
		target = provider("http://provider.comet.fi.fao.org/" + targetProvider.getClass().getName()).of("http://data.comet.fi.fao.org/" + targetType.getName()).named("urn:provider#target");

		return convert(config, source, target, mappings, sourceType, targetType, identifier, version, author, description, verbose);
	}
	
	
	public CometMappingData convert(MatchingEngineProcessConfiguration config,
									CometDataProvider sourceProvider, 
									CometDataProvider targetProvider, 
									MatchingsData<SOURCE, TARGET> mappings, 
									Class<SOURCE> sourceType,
									Class<TARGET> targetType,
									String identifier,
									String version,
									String author,
									String description,
									boolean verbose) throws URISyntaxException {
		URI mappingId = null;
		
		mappingId = new URI(identifier);

		CometMappingData mappingData = CometMappingData.
			withId(mappingId).
			version(version).
			producedBy(author).
			now().
			with(config.getMinimumAllowedWeightedScore(), config.getMaxCandidatesPerEntry()).
			linking(sourceProvider).to(targetProvider);

		if(verbose) {
			mappingData.setMatchletConfiguration(new ArrayList<CometMatchletConfiguration>());

			for(MatchletConfiguration matchlet : config.getMatchletsConfigurations()) {
				mappingData.getMatchletConfiguration().add(convertConfiguration(matchlet));
			}
		}

		mappingData.setDescription(description);

		CometMapping currentMapping;
		CometMappingDetail currentMappingDetail = null;

		for(MatchingDetails<SOURCE, TARGET> in : mappings.getMatchingDetails()) {
			currentMapping = map(wrap(asElement(verbose ? in.getSource() : null)).with(identifierFor("urn:" + in.getSourceId())));

			for(Matching<SOURCE, TARGET> currentTarget : in.getMatchings()) {
				currentMappingDetail = target(wrap(asElement(verbose ? currentTarget.getTarget() : null)).with(identifierFor("urn:" + currentTarget.getTargetId()))).withMappingScore(currentTarget.getScoreValue(), CometMappingScoreType.valueOf(currentTarget.getScoreType().name()));

				if(verbose) {
					for(MatchingResult<?, ?> currentResult : currentTarget.getMatchingResults()) {
						currentMappingDetail.and(
							matchlet(currentResult.getOriginatingMatchletId()).scoring(currentResult.getMatchletScoreValue(), CometMappingScoreType.valueOf(currentResult.getMatchletScoreType().name()))
						);
					}
				}

				currentMapping = currentMapping.andTo(currentMappingDetail);
			}
			
			mappingData.include(currentMapping);
		}
		
		return mappingData;
	}

	private CometMatchletConfiguration convertConfiguration(MatchletConfiguration matchlet) throws URISyntaxException {
		CometMatchletConfiguration matcher = new CometMatchletConfiguration();
		matcher.setMatchletId(new URI(matchlet.getMatchletId()));
		matcher.setMatchletType(matchlet.getMatchletName());
		matcher.setConfigurationProperties(new ArrayList<CometMatchletConfigurationProperty>());
	
		String parameterName;
		for(MatchletConfigurationParameter in : matchlet.getMatchletParameters()) {
			parameterName = in.getName();
	
			if(Matchlet.WEIGHT_PARAM.equals(parameterName))
				matcher.setWeight(Double.valueOf(in.getValue()));
			else if(Matchlet.OPTIONAL_PARAM.equals(parameterName))
				matcher.setOptional(Boolean.valueOf(in.getValue()));
			else if(Matchlet.MIN_SCORE_PARAM.equals(parameterName))
				matcher.setMinimumScore(Double.valueOf(in.getValue()));
			else
				matcher.getConfigurationProperties().add(new CometMatchletConfigurationProperty(in.getName(), null, null, in.getValue()));
		}
	
		return matcher;
	}
}