/**
 * (c) 2013 FAO / UN (project: vrmf-comparison-engine-core)
 */
package org.fao.fi.comet.bridge.test.converter;

import static org.fao.fi.comet.core.model.matchlets.Matchlet.FORCE_COMPARISON_PARAM;
import static org.fao.fi.comet.core.model.matchlets.Matchlet.MIN_SCORE_PARAM;
import static org.fao.fi.comet.core.model.matchlets.Matchlet.OPTIONAL_PARAM;
import static org.fao.fi.comet.core.model.matchlets.Matchlet.WEIGHT_PARAM;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.JAXBContext;
import javax.xml.transform.stream.StreamSource;

import org.fao.fi.comet.bridge.converter.CometFormatConverter;
import org.fao.fi.comet.bridge.mapping.model.CometMappingData;
import org.fao.fi.comet.bridge.mapping.model.utils.jaxb.JAXBDeSerializationUtils;
import org.fao.fi.comet.bridge.test.converter.support.data.SimpleMappableData;
import org.fao.fi.comet.core.engine.MatchingEngineCore;
import org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler;
import org.fao.fi.comet.core.engine.process.handlers.impl.SilentMatchingProcessHandler;
import org.fao.fi.comet.core.io.providers.impl.basic.CollectionBackedDataProvider;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.patterns.data.partitioners.impl.IdentityDataPartitioner;
import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.extras.patterns.handlers.id.impl.basic.IdentifiableDataIntegerIDHandler;
import org.fao.fi.sh.utility.common.helpers.singletons.text.XMLHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.io.MD5Helper;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14/mar/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 14/mar/2013
 */
public class SimpleMatchingEngineAndConversionTest {
	final private MatchingEngineProcessConfiguration CONFIGURATION = configure(.5, 2);
	
	private MatchingEngineCore<SimpleMappableData, SimpleMappableData, MatchingEngineProcessConfiguration> getEngine() {
		return new MatchingEngineCore<SimpleMappableData, SimpleMappableData, MatchingEngineProcessConfiguration>();
	}

	private MatchingEngineCore<SimpleMappableData, SimpleMappableData, MatchingEngineProcessConfiguration> getEngine(int numThreads) {
		return new MatchingEngineCore<SimpleMappableData, SimpleMappableData, MatchingEngineProcessConfiguration>(numThreads);
	}

	private Collection<MatchletConfiguration> getMatchletsConfiguration() {
		Collection<MatchletConfiguration> matchlets = new ArrayList<MatchletConfiguration>();
		matchlets.add(MatchletConfiguration.configure("SimpleMappableDataMatchlet").
													  with(WEIGHT_PARAM, 100D).
													  with(MIN_SCORE_PARAM, .5).
													  with("delta", 1).
													  withFalse(FORCE_COMPARISON_PARAM).
													  withFalse(OPTIONAL_PARAM));

		matchlets.add(MatchletConfiguration.configure("SimpleMappableDataMatchlet").
													  with(WEIGHT_PARAM, 50D).
													  with(MIN_SCORE_PARAM, .5).
													  with("delta", 2).
													  withFalse(FORCE_COMPARISON_PARAM).
													  withFalse(OPTIONAL_PARAM));

		return matchlets;
	}

	private MatchingEngineProcessConfiguration configure(double minScore, int maxCandidates) {
		MatchingEngineProcessConfiguration config = new MatchingEngineProcessConfiguration();
		config.setMinimumAllowedWeightedScore(minScore);
		config.setMaxCandidatesPerEntry(maxCandidates);
		config.setHaltAtFirstValidMatching(false);

		config.setMatchletsConfiguration(getMatchletsConfiguration());

		return config;
	}

	private DataProvider<SimpleMappableData> getDataProvider(int initialId, int finalId) {
		Collection<SimpleMappableData> dataset = new ListSet<SimpleMappableData>();

		char data;
		for(int c=initialId; c<=finalId; c++) {
			data = (char)('a' + c - 1);
			dataset.add(new SimpleMappableData(new Integer(c), new Character(data)));
		}

		return new CollectionBackedDataProvider<SimpleMappableData>(dataset);
	}
	
	@Test
	public void testThreadedEngineAndConversion() throws Throwable {
		processAndConvert(CONFIGURATION, 8);
	}

	@Test
	public void testSequentialEngineAndConversion() throws Throwable {
		processAndConvert(CONFIGURATION, 1);
	}
	
	@Test
	public void testRoundTrip() throws Throwable {
		File produced = processAndConvert(CONFIGURATION, 8);
		
		String checksum = MD5Helper.digest(produced);
		
		CometMappingData rebuilt = JAXBDeSerializationUtils.fromSource(
			new StreamSource(new FileInputStream(produced)),
			JAXBContext.newInstance(
				new Class[] {
					//SimpleMappableData.class,
					CometMappingData.class
				}
			)
		);
		
		String toXml = XMLHelper.prettyPrint(JAXBDeSerializationUtils.toXML(
			rebuilt,
			JAXBContext.newInstance(
				new Class[] {
					SimpleMappableData.class,
					CometMappingData.class
				}
			)
		));
		
		String rebuiltChecksum = MD5Helper.digest(toXml);
		
		Assert.assertEquals(checksum, rebuiltChecksum);
	}
	
	private File processAndConvert(MatchingEngineProcessConfiguration conf, int numThreads) throws Throwable {
		MatchingEngineProcessResult<SimpleMappableData, SimpleMappableData, MatchingEngineProcessConfiguration> result = runEngine(conf, numThreads);
		
		long fileId = System.currentTimeMillis();
		
		convert(CONFIGURATION, result, fileId, false);
		
		return convert(CONFIGURATION, result, fileId, true);
	}
	
	private MatchingEngineProcessResult<SimpleMappableData, SimpleMappableData, MatchingEngineProcessConfiguration> runEngine(MatchingEngineProcessConfiguration conf, int numThreads) throws Throwable {
		MatchingEngineCore<SimpleMappableData, SimpleMappableData, MatchingEngineProcessConfiguration> engine = numThreads > 0 ? getEngine(numThreads) : getEngine();
		
		MatchingProcessHandler<SimpleMappableData, SimpleMappableData> tracker = null;

		tracker = new SilentMatchingProcessHandler<SimpleMappableData, SimpleMappableData>();

		DataProvider<SimpleMappableData> sourceProvider = getDataProvider(1, 900);
		DataProvider<SimpleMappableData> targetProvider = getDataProvider(1, 900);
		
		MatchingEngineProcessResult<SimpleMappableData, SimpleMappableData, MatchingEngineProcessConfiguration> result = engine.compareAll(
			conf,
			tracker,
			sourceProvider,
			new IdentityDataPartitioner<SimpleMappableData, SimpleMappableData>(),
			targetProvider,
			new IdentifiableDataIntegerIDHandler<SimpleMappableData>(),
			new IdentifiableDataIntegerIDHandler<SimpleMappableData>()
		);

		System.out.println(numThreads + " threads - Total Errors: " + tracker.getNumberOfComparisonErrors());
		System.out.println(numThreads + " threads - Total Matches: " + tracker.getTotalNumberOfMatches());
		System.out.println(numThreads + " threads - Matches: " + tracker.getNumberOfMatches());
		System.out.println(numThreads + " threads - Auth. full matches: " + tracker.getNumberOfAuthoritativeFullMatches());
		System.out.println(numThreads + " threads - Elapsed: " + tracker.getElapsed());
		System.out.println(numThreads + " threads - Atomic comparisons: " + tracker.getTotalNumberOfAtomicComparisonsPerformed());
		System.out.println(numThreads + " threads - Atomic throughput: " + tracker.getAtomicThroughput() * 1000 + " atomic comparisons per second");
		System.out.println(numThreads + " threads - Throughput: " + tracker.getThroughput() * 1000 + " comparisons per second");
		
		return result;
	}

	private File convert(MatchingEngineProcessConfiguration configuration, MatchingEngineProcessResult<SimpleMappableData, SimpleMappableData, MatchingEngineProcessConfiguration> result, long fileId, boolean verbose) throws Throwable {
		CometFormatConverter<SimpleMappableData, SimpleMappableData, MatchingEngineProcessConfiguration> converter = new CometFormatConverter<SimpleMappableData, SimpleMappableData, MatchingEngineProcessConfiguration>(); 
		
		DataProvider<SimpleMappableData> sourceProvider = getDataProvider(1, 900);
		DataProvider<SimpleMappableData> targetProvider = getDataProvider(1, 900);
		
		CometMappingData mappings = converter.convert(
			configuration, 
			sourceProvider, 
			targetProvider, 
			result.getResults(),
			SimpleMappableData.class,
			SimpleMappableData.class,
			"foo", 
			"6.66", 
			"Foo Bazzi", 
			"Yet another (" + ( verbose ? "verbose" : "barebone" ) + ") test mapping", 
			verbose
		);
		
		String xml = XMLHelper.prettyPrint(JAXBDeSerializationUtils.toXML(
			mappings,
			JAXBContext.newInstance(
				new Class[] {
					SimpleMappableData.class,
					CometMappingData.class
				}
			)
		));
		
		File outFile = new File("src/test/out/comet_" + fileId + "_" + ( verbose ? "verbose" : "barebone" ) + ".xml");
		
		try(FileOutputStream fos = new FileOutputStream(outFile)) {
			fos.write(xml.getBytes());
		}
		
		System.out.println("Result has been written to file " + outFile.getAbsolutePath());
		
		return outFile;
	}
}