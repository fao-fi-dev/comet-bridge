/**
 * (c) 2013 FAO / UN (project: vrmf-comparison-engine-core)
 */
package org.fao.fi.comet.bridge.test.converter.support.matchlets;

import org.fao.fi.comet.bridge.test.converter.support.data.SimpleMappableData;
import org.fao.fi.comet.core.matchlets.skeleton.ScalarMatchletSkeleton;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletParameter;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 15 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 15 Mar 2013
 */
public class SimpleMappableDataMatchlet extends ScalarMatchletSkeleton<SimpleMappableData, Character, SimpleMappableData, Character> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6116412804832997470L;

	@MatchletParameter(name="delta")
	private int _delta = 1;

	/**
	 * Class constructor
	 *
	 * @param delta
	 */
	public SimpleMappableDataMatchlet(int delta) {
		super();

		this._delta = delta;
		this._name = "SimpleMappableDataMatchlet";
	}

	public SimpleMappableDataMatchlet() {
		super();

		this._name = "SimpleMappableDataMatchlet";
	}

	/**
	 * @return the 'delta' value
	 */
	public int getDelta() {
		return this._delta;
	}

	/**
	 * @param delta the 'delta' value to set
	 */
	public void setDelta(int delta) {
		this._delta = delta;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Fake " + this.getClass().getSimpleName() + " description";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.ScalarMatchlet#extractSourceData(java.io.Serializable)
	 */
	@Override
	public Character extractSourceData(SimpleMappableData source, DataIdentifier sourceIdentifier) {
		return source.getData();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.ScalarMatchlet#extractTargetData(java.io.Serializable)
	 */
	@Override
	public Character extractTargetData(SimpleMappableData target, DataIdentifier targetIdentifier) {
		return target.getData();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#computeScore(java.io.Serializable, java.io.Serializable)
	 */
	@Override
	public MatchingScore computeScore(SimpleMappableData source, DataIdentifier sourceIdentifier, Character sourceData, SimpleMappableData target, DataIdentifier targetIdentifier, Character targetData) {
		if(sourceData.equals(targetData))
			return MatchingScore.getAuthoritativeFullMatchTemplate();

		if(targetData == null || sourceData == null)
			return MatchingScore.getNonPerformedTemplate();

		char sourceChar = sourceData.charValue();
		char targetChar = targetData.charValue();

		int delta = Math.abs(sourceChar - targetChar);

		if(delta >=1 && delta <= this._delta)
			return new MatchingScore(1.0 / delta, MatchingType.NON_AUTHORITATIVE);

		return MatchingScore.getNonAuthoritativeNoMatchTemplate();
	}
}