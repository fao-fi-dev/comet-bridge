/**
 * (c) 2014 FAO / UN (project: comet-mapping-format)
 */
package org.fao.fi.comet.bridge.test.mapping.model;

import static org.fao.fi.comet.bridge.mapping.dsl.MappingDSL.map;
import static org.fao.fi.comet.bridge.mapping.dsl.MappingDetailDSL.target;
import static org.fao.fi.comet.bridge.mapping.dsl.MappingElementDSL.wrap;
import static org.fao.fi.comet.bridge.mapping.dsl.MappingElementIdentifierDSL.identifierFor;
import static org.fao.fi.comet.bridge.mapping.model.utils.jaxb.JAXB2DOMUtils.asElement;

import java.util.ArrayList;
import java.util.Collection;

import org.fao.fi.comet.bridge.mapping.model.CometMapping;
import org.fao.fi.comet.bridge.mapping.model.CometMappingData;
import org.fao.fi.comet.bridge.mapping.model.CometMappingDetail;
import org.fao.fi.comet.bridge.mapping.model.CometMappingElement;
import org.fao.fi.comet.bridge.mapping.model.utils.jaxb.JAXBDeSerializationUtils;
import org.fao.fi.comet.bridge.test.mapping.model.support.GenericTerm;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 May 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 May 2014
 */
public class TestProgrammaticMappingCreation {
	@Test
	public void testCreationWithDSL() {
		CometMappingData mappingData = new CometMappingData();

		CometMapping mappingEntry;

		CometMappingElement target, source = wrap(asElement(GenericTerm.describing("srcFoo1"))).with(identifierFor("urn:src:foo:1"));

		mappingEntry = map(source);

		for (int i = 0; i < 10; i++) {
			target = wrap(asElement(GenericTerm.describing("trgfoo_" + i))).with(identifierFor("urn:trg:foo:" + i));

			mappingEntry = mappingEntry.to(target(target));
		}

		mappingData.include(mappingEntry);
		
		source = wrap(asElement(GenericTerm.describing("srcFoo2"))).with(identifierFor("urn:src:foo:2"));
		
		mappingEntry = map(source);

		for (int i = 0; i < 8; i++) {
			target = wrap(asElement(GenericTerm.describing("trgfoo_" + i))).with(identifierFor("urn:trg:foo:" + i));

			mappingEntry = mappingEntry.to(target(target));
		}

		mappingData.include(mappingEntry);
		
		CometMapping[] mappings = mappingData.getMappings().toArray(new CometMapping[0]);
		
		Assert.assertEquals(2, mappings.length);
		Assert.assertEquals(10, mappings[0].getTargets().size());
		Assert.assertEquals(8, mappings[1].getTargets().size());
		
		System.out.println(JAXBDeSerializationUtils.toXML(mappingData));
	}
	
	@Test
	public void testCreationWithGettersSetters() {
		CometMappingData mappingData = new CometMappingData();
		
		Collection<CometMapping> allMappings = new ArrayList<CometMapping>();

		mappingData.setMappings(allMappings);
		
		CometMappingElement target, source = wrap(asElement(GenericTerm.describing("srcFoo1"))).with(identifierFor("urn:src:foo:1"));
		
		Collection<CometMappingDetail> targetMappingDetails = new ArrayList<CometMappingDetail>();
		
		CometMappingDetail detail;
		for (int i = 0; i < 10; i++) {
			detail = new CometMappingDetail();
			
			target = wrap(asElement(GenericTerm.describing("trgfoo_" + i))).with(identifierFor("urn:trg:foo:" + i));
			detail.setTargetElement(target);
			
			targetMappingDetails.add(detail);
		}
		
		allMappings.add(new CometMapping(source, targetMappingDetails));
		
		source = wrap(asElement(GenericTerm.describing("srcFoo2"))).with(identifierFor("urn:src:foo:2"));
		
		targetMappingDetails = new ArrayList<CometMappingDetail>();

		for (int i = 0; i < 8; i++) {
			detail = new CometMappingDetail();
			
			target = wrap(asElement(GenericTerm.describing("trgfoo_" + i))).with(identifierFor("urn:trg:foo:" + i));
			detail.setTargetElement(target);
			
			targetMappingDetails.add(detail);
		}
		
		allMappings.add(new CometMapping(source, targetMappingDetails));
		
		CometMapping[] mappings = mappingData.getMappings().toArray(new CometMapping[0]);
		
		Assert.assertEquals(2, mappings.length);
		Assert.assertEquals(10, mappings[0].getTargets().size());
		Assert.assertEquals(8, mappings[1].getTargets().size());
		
		System.out.println(JAXBDeSerializationUtils.toXML(mappingData));
	}
}
