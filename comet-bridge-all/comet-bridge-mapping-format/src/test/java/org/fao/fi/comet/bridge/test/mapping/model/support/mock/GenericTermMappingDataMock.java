/**
 * (C) 2014 by FAO / UN (project: comet-mapping-format)
 */
package org.fao.fi.comet.bridge.test.mapping.model.support.mock;

import static org.fao.fi.comet.bridge.mapping.dsl.DataProviderDSL.provider;
import static org.fao.fi.comet.bridge.mapping.dsl.MappingContributionDSL.matchlet;
import static org.fao.fi.comet.bridge.mapping.dsl.MappingDSL.map;
import static org.fao.fi.comet.bridge.mapping.dsl.MappingDataDSL.maximumCandidates;
import static org.fao.fi.comet.bridge.mapping.dsl.MappingDataDSL.minimumWeightedScore;
import static org.fao.fi.comet.bridge.mapping.dsl.MappingDetailDSL.target;
import static org.fao.fi.comet.bridge.mapping.dsl.MappingElementDSL.nil;
import static org.fao.fi.comet.bridge.mapping.dsl.MappingElementDSL.wrap;
import static org.fao.fi.comet.bridge.mapping.dsl.MappingElementIdentifierDSL.identifierFor;
import static org.fao.fi.comet.bridge.mapping.dsl.MatchletConfigurationDSL.configuredMatchlet;
import static org.fao.fi.comet.bridge.mapping.dsl.MatchletConfigurationDSL.optional;
import static org.fao.fi.comet.bridge.mapping.dsl.MatchletConfigurationPropertyDSL.configurationProperty;
import static org.fao.fi.comet.bridge.mapping.model.utils.jaxb.JAXB2DOMUtils.asElement;

import org.fao.fi.comet.bridge.mapping.model.CometDataProvider;
import org.fao.fi.comet.bridge.mapping.model.CometMappingData;
import org.fao.fi.comet.bridge.test.mapping.model.support.GenericTerm;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17/apr/2014   Fabio	 Creation.
 *
 * @version 1.0
 * @since 17/apr/2014
 */
public class GenericTermMappingDataMock extends CometMappingData {
	final static public long serialVersionUID = -666;
	
	private GenericTermMappingDataMock() {
		super();
	}
	
	static final public CometMappingData newInstance() throws Throwable {
		CometDataProvider sourceDataProvider = provider("http://cotrix.org").of("http://cotrix.org/codelist").named("urn:fooResourceStatus").withVersion("1.0");
		CometDataProvider targetDataProvider = provider("http://cotrix.org").of("http://cotrix.org/codelist").named("urn:barResourceStatus").withVersion("1.1");
		
		CometMappingData mappingData = CometMappingData.
			withId("urn:foo:bar").
			version("0.01").
			producedBy("Foo Bazzi").
			now().
			linking(sourceDataProvider).to(targetDataProvider).
			through(
				configuredMatchlet("urn:matchlet:foo").
					ofType("org.fao.fi.comet.fake.matchlets.LexicalMatchlet").
					weighting(10).
					withMinimumScore(0.1).
					having(
						configurationProperty("stripSymbols", Boolean.FALSE)
					),
				optional(
					configuredMatchlet("urn:matchlet:bar").
						ofType("org.fao.fi.comet.fake.matchlets.AnotherLexicalMatchlet").
						weighting(30).
						withMinimumScore(0.0).
						having(
							configurationProperty("useSoundex", Boolean.TRUE),
							configurationProperty("stripSymbols", Boolean.TRUE)
						)
					),
				optional(
					configuredMatchlet("urn:matchlet:baz").
						ofType("org.fao.fi.comet.fake.matchlets.YetAnotherLexicalMatchlet").
						weighting(20).
						withMinimumScore(0.2).
						having(
							configurationProperty("useSoundex", Boolean.TRUE)
						)
					)
			).
			with(minimumWeightedScore(0.3), maximumCandidates(5)).
			including(
				map(wrap(asElement(GenericTerm.describing("over-exploited"))).with(identifierFor("urn:1"))).
					to(
						target(wrap(asElement(GenericTerm.describing("overexploited"))).with(identifierFor("urn:69"))).
							asContributedBy(matchlet("urn:matchlet:foo").scoring(0.39), 
											matchlet("urn:matchlet:bar").scoring(0.69),
											matchlet("urn:matchlet:baz").nonPerformed()
							).withWeightedScore(0.59)
					).andTo(
						target(wrap(asElement(GenericTerm.describing("ov-erexploited"))).with(identifierFor("urn:96"))).
							asContributedBy(matchlet("urn:matchlet:foo").scoring(0.79), 
											matchlet("urn:matchlet:bar").nonPerformed(),
											matchlet("urn:matchlet:baz").nonPerformed()
							).withWeightedScore(0.59)
					)
			).including(
				map(wrap(asElement(GenericTerm.describing("under-exploited"))).with(identifierFor("urn:2"))).
					to(
						target(wrap(asElement(GenericTerm.describing("underexploited"))).with(identifierFor("urn:70"))).
							asContributedBy(matchlet("urn:matchlet:foo").scoring(0.49), 
											matchlet("urn:matchlet:bar").scoring(0.59),
											matchlet("urn:matchlet:baz").nonPerformed()
							).withWeightedScore(0.39)
					).andTo(
						target(wrap(asElement(GenericTerm.describing("und-erexploited"))).with(identifierFor("urn:97"))).
							asContributedBy(matchlet("urn:matchlet:foo").scoring(0.79), 
											matchlet("urn:matchlet:bar").scoringNoMatch(),
											matchlet("urn:matchlet:baz").nonPerformed()
							).withWeightedScore(0.79)
					).andTo(
						target(wrap(asElement(GenericTerm.describing("un-derexploited"))).with(identifierFor("urn:98"))).
							asContributedBy(matchlet("urn:matchlet:foo").scoringFullMatch(), 
											matchlet("urn:matchlet:bar").nonPerformed(),
											matchlet("urn:matchlet:baz").scoring(0.39)
							).withWeightedScore(0.35)
					)
		);
		
		return mappingData;
	}
	
	static final public CometMappingData newInstanceWithNils() throws Throwable {
		CometDataProvider sourceDataProvider = provider("http://cotrix.org").of("http://cotrix.org/codelist").named("urn:fooResourceStatus").withVersion("1.0");
		CometDataProvider targetDataProvider = provider("http://cotrix.org").of("http://cotrix.org/codelist").named("urn:barResourceStatus").withVersion("1.1");
		
		CometMappingData mappingData = CometMappingData.
			withId("urn:foo:bar").
			version("0.01").
			producedBy("Foo Bazzi").
			now().
			linking(sourceDataProvider).to(targetDataProvider).
			including(
				map(nil().with(identifierFor("urn:1"))).
					to(
						target(nil().with(identifierFor("urn:69")))
					).andTo(
						target(nil().with(identifierFor("urn:96")))
					)
			).including(
				map(nil().with(identifierFor("urn:2"))).
					to(
						target(nil().with(identifierFor("urn:70")))
					).andTo(
						target(nil().with(identifierFor("urn:97")))
					).andTo(
						target(nil().with(identifierFor("urn:98")))
					)
		);
		
		return mappingData;
	}
}