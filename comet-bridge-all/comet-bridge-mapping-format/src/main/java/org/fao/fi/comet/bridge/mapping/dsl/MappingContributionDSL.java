/**
 * (c) 2014 FAO / UN (project: comet-mapping-format)
 */
package org.fao.fi.comet.bridge.mapping.dsl;

import java.net.URI;

import org.fao.fi.comet.bridge.mapping.model.CometMappingContribution;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Apr 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Apr 2014
 */
public class MappingContributionDSL {
	final static public CometMappingContribution matchlet(URI matchletId) {
		return new CometMappingContribution(matchletId, 0D, null);
	}
	
	final static public CometMappingContribution matchlet(String matchletIdURI) {
		return MappingContributionDSL.matchlet(URI.create(matchletIdURI));
	}
}
