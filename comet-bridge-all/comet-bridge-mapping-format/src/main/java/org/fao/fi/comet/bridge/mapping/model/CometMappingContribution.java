/**
 * (c) 2014 FAO / UN (project: comet-mapping-format)
 */
package org.fao.fi.comet.bridge.mapping.model;

import java.io.Serializable;
import java.net.URI;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.comet.bridge.mapping.ScoreValue;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Apr 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Apr 2014
 */
@XmlType(name="CometMappingContribution")
@XmlAccessorType(XmlAccessType.FIELD)
public class CometMappingContribution implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8748101803408206932L;

	@XmlAttribute(name="matchletScoreType") private CometMappingScoreType _matchletScoreType;
	@XmlAttribute(name="matchletScore") private double _matchletScore;
	@XmlAttribute(name="matchletId") private URI _matchletId;
	
	/**
	 * Class constructor
	 *
	 */
	public CometMappingContribution() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param matchletId
	 * @param matchletScore
	 * @param matchletScoreType
	 */
	public CometMappingContribution(URI matchletId, double matchletScore, CometMappingScoreType matchletScoreType) {
		super();
		this._matchletId = matchletId;
		this._matchletScore = matchletScore;
		this._matchletScoreType = matchletScoreType;
	}
	
	
	/**
	 * @return the 'matchletId' value
	 */
	public URI getMatchletId() {
		return this._matchletId;
	}

	/**
	 * @param matchletId the 'matchletId' value to set
	 */
	public void setMatchletId(URI matchletId) {
		this._matchletId = matchletId;
	}

	/**
	 * @return the 'matchletScore' value
	 */
	public double getMatchletScore() {
		return this._matchletScore;
	}

	/**
	 * @param matchletScore the 'matchletScore' value to set
	 */
	public void setMatchletScore(double matchletScore) {
		this._matchletScore = matchletScore;
	}

	/**
	 * @return the 'matchletScoreType' value
	 */
	public CometMappingScoreType getMatchletScoreType() {
		return this._matchletScoreType;
	}

	/**
	 * @param matchletScoreType the 'matchletScoreType' value to set
	 */
	public void setMatchletScoreType(CometMappingScoreType matchletScoreType) {
		this._matchletScoreType = matchletScoreType;
	}
	
	public CometMappingContribution scoring(double score, CometMappingScoreType scoreType) {
		this._matchletScore = score;
		this._matchletScoreType = scoreType;
		
		return this;
	}
	
	public CometMappingContribution scoring(double score) {
		return this.scoring(score, CometMappingScoreType.NON_AUTHORITATIVE);
	}
	
	public CometMappingContribution nonPerformed() {
		return this.scoring(ScoreValue.NO_MATCH, CometMappingScoreType.NON_PERFORMED);
	}
	
	public CometMappingContribution scoringAuthoritative(double score) {
		return this.scoring(score, CometMappingScoreType.AUTHORITATIVE);
	}

	public CometMappingContribution scoringNonAuthoritative(double score) {
		return this.scoring(score, CometMappingScoreType.NON_AUTHORITATIVE);
	}

	public CometMappingContribution scoringAuthoritativeFullMatch() {
		return this.scoringAuthoritative(ScoreValue.FULL_MATCH);
	}
	
	public CometMappingContribution scoringAuthoritativeNoMatch() {
		return this.scoringAuthoritative(ScoreValue.NO_MATCH);
	}

	public CometMappingContribution scoringFullMatch() {
		return this.scoringNonAuthoritative(ScoreValue.FULL_MATCH);
	}

	public CometMappingContribution scoringNoMatch() {
		return this.scoringNonAuthoritative(ScoreValue.NO_MATCH);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._matchletId == null) ? 0 : this._matchletId.hashCode());
		long temp;
		temp = Double.doubleToLongBits(this._matchletScore);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((this._matchletScoreType == null) ? 0 : this._matchletScoreType.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CometMappingContribution other = (CometMappingContribution) obj;
		if (this._matchletId == null) {
			if (other._matchletId != null)
				return false;
		} else if (!this._matchletId.equals(other._matchletId))
			return false;
		if (Double.doubleToLongBits(this._matchletScore) != Double.doubleToLongBits(other._matchletScore))
			return false;
		if (this._matchletScoreType != other._matchletScoreType)
			return false;
		return true;
	}
}