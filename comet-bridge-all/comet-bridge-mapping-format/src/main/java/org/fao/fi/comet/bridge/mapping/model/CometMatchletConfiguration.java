/**
 * (c) 2014 FAO / UN (project: comet-mapping-format)
 */
package org.fao.fi.comet.bridge.mapping.model;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.comet.bridge.mapping.ScoreValue;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Apr 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Apr 2014
 */
@XmlType(name="CometMatchletConfiguration")
@XmlAccessorType(XmlAccessType.FIELD)
public class CometMatchletConfiguration implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5759769021546429122L;

	@XmlAttribute(name="isOptional") private boolean _isOptional;
	@XmlAttribute(name="minimumScore") private double _minimumScore;
	@XmlAttribute(name="weight") private double _weight;
	@XmlAttribute(name="type") private String _type;
	@XmlAttribute(name="id") private URI _id;
	
	@XmlElement(name="ConfigurationProperty")
	private Collection<CometMatchletConfigurationProperty> _configurationProperties;
	
	/**
	 * Class constructor
	 */
	public CometMatchletConfiguration() {
		super();
	}
	
	/**
	 * Class constructor
	 */
	public CometMatchletConfiguration(URI id) {
		this(id, null, 1D, ScoreValue.NO_MATCH, false, new ArrayList<CometMatchletConfigurationProperty>());
	}
	
	/**
	 * Class constructor
	 */
	public CometMatchletConfiguration(String idURI) {
		this(URI.create(idURI));
	}

	/**
	 * Class constructor
	 *
	 * @param matchletId
	 * @param matchletType
	 * @param weight
	 * @param minimumScore
	 * @param isOptional
	 * @param configurationProperties
	 */
	public CometMatchletConfiguration(URI matchletId, String matchletType, double weight, double minimumScore, boolean isOptional, Collection<CometMatchletConfigurationProperty> configurationProperties) {
		super();
		this._id = matchletId;
		this._type = matchletType;
		this._weight = weight;
		this._minimumScore = minimumScore;
		this._isOptional = isOptional;
		this._configurationProperties = configurationProperties;
	}

	/**
	 * Class constructor
	 *
	 * @param matchletId
	 * @param matchletType
	 * @param weight
	 * @param minimumScore
	 * @param isOptional
	 * @param configurationProperties
	 */
	public CometMatchletConfiguration(String matchletId, String matchletType, double weight, double minimumScore, boolean isOptional, Collection<CometMatchletConfigurationProperty> configurationProperties) {
		this(URI.create(matchletId), matchletType, weight, minimumScore, isOptional, configurationProperties);
	}

	/**
	 * @return the 'matchletId' value
	 */
	public URI getMatchletId() {
		return this._id;
	}

	/**
	 * @param matchletId the 'matchletId' value to set
	 */
	public void setMatchletId(URI matchletId) {
		this._id = matchletId;
	}

	/**
	 * @return the 'matchletType' value
	 */
	public String getMatchletType() {
		return this._type;
	}

	/**
	 * @param matchletType the 'matchletType' value to set
	 */
	public void setMatchletType(String matchletType) {
		this._type = matchletType;
	}
	
	/**
	 * @return the 'weight' value
	 */
	public double getWeight() {
		return this._weight;
	}

	/**
	 * @param weight the 'weight' value to set
	 */
	public void setWeight(double weight) {
		this._weight = weight;
	}

	/**
	 * @return the 'minimumScore' value
	 */
	public double getMinimumScore() {
		return this._minimumScore;
	}

	/**
	 * @param minimumScore the 'minimumScore' value to set
	 */
	public void setMinimumScore(double minimumScore) {
		this._minimumScore = minimumScore;
	}

	/**
	 * @return the 'isOptional' value
	 */
	public boolean isOptional() {
		return this._isOptional;
	}

	/**
	 * @param isOptional the 'isOptional' value to set
	 */
	public void setOptional(boolean isOptional) {
		this._isOptional = isOptional;
	}

	/**
	 * @return the 'configurationProperties' value
	 */
	public Collection<CometMatchletConfigurationProperty> getConfigurationProperties() {
		return this._configurationProperties;
	}

	/**
	 * @param configurationProperties the 'configurationProperties' value to set
	 */
	public void setConfigurationProperties(Collection<CometMatchletConfigurationProperty> configurationProperties) {
		this._configurationProperties = configurationProperties;
	}
	
	public CometMatchletConfiguration having(CometMatchletConfigurationProperty... configurationProperties) {
		this.setConfigurationProperties(Arrays.asList(configurationProperties));
		
		return this;
	}
	
	public CometMatchletConfiguration weighting(double weight) {
		this._weight = weight;
		
		return this;
	}
	
	public CometMatchletConfiguration ofType(String type) {
		this._type = type;
		
		return this;
	}
	
	public CometMatchletConfiguration optional() {
		this._isOptional = true;
		
		return this;
	}
	
	public CometMatchletConfiguration withMinimumScore(double minimumScore) {
		this._minimumScore = minimumScore;
		
		return this;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._configurationProperties == null) ? 0 : this._configurationProperties.hashCode());
		result = prime * result + (this._isOptional ? 1231 : 1237);
		result = prime * result + ((this._id == null) ? 0 : this._id.hashCode());
		result = prime * result + ((this._type == null) ? 0 : this._type.hashCode());
		long temp;
		temp = Double.doubleToLongBits(this._minimumScore);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(this._weight);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CometMatchletConfiguration other = (CometMatchletConfiguration) obj;
		if (this._configurationProperties == null) {
			if (other._configurationProperties != null)
				return false;
		} else if (!this._configurationProperties.equals(other._configurationProperties))
			return false;
		if (this._isOptional != other._isOptional)
			return false;
		if (this._id == null) {
			if (other._id != null)
				return false;
		} else if (!this._id.equals(other._id))
			return false;
		if (this._type == null) {
			if (other._type != null)
				return false;
		} else if (!this._type.equals(other._type))
			return false;
		if (Double.doubleToLongBits(this._minimumScore) != Double.doubleToLongBits(other._minimumScore))
			return false;
		if (Double.doubleToLongBits(this._weight) != Double.doubleToLongBits(other._weight))
			return false;
		return true;
	}
}