/**
 * (c) 2014 FAO / UN (project: comet-mapping-format)
 */
package org.fao.fi.comet.bridge.mapping.model.utils.jaxb;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMResult;

import org.fao.fi.comet.bridge.mapping.model.CometMappingData;
import org.w3c.dom.Document;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 May 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 May 2014
 */
final public class JAXBDeSerializationUtils {
	final static private JAXBContext MINIMAL_JAXB_CONTEXT;
	
	static {
		try {
			MINIMAL_JAXB_CONTEXT = JAXBContext.newInstance(CometMappingData.class);
		} catch(JAXBException Je) {
			throw new RuntimeException("Unable to create minimal JAXB Context instance: " + Je.getMessage(), Je);
		}
	}
	
	static public void toResult(CometMappingData mappingData, Result result) {
		JAXBDeSerializationUtils.toResult(mappingData, result, MINIMAL_JAXB_CONTEXT);
	}
	
	static public void toResult(CometMappingData mappingData, Result result, JAXBContext context) {
		try {
			context.createMarshaller().marshal(mappingData, result);
		} catch(Throwable t) {
			throw new RuntimeException("Unable to marshal " + mappingData + ": " + t.getMessage(), t);
		}
	}
	
	static public Document toDocument(CometMappingData mappingData) {
		return JAXBDeSerializationUtils.toDocument(mappingData, MINIMAL_JAXB_CONTEXT);
	}
	
	static public Document toDocument(CometMappingData mappingData, JAXBContext context) {
		try {
			DOMResult domResult;
			
			context.createMarshaller().marshal(mappingData, domResult = new DOMResult());
			
			return (Document)domResult.getNode();
		} catch(Throwable t) {
			throw new RuntimeException("Unable to marshal " + mappingData + ": " + t.getMessage(), t);
		}
	}
	
	static public CometMappingData fromSource(Source source) {
		return JAXBDeSerializationUtils.fromSource(source, MINIMAL_JAXB_CONTEXT);
	}
	
	static public CometMappingData fromSource(Source source, JAXBContext context) {
		try {
			return context.createUnmarshaller().unmarshal(source, CometMappingData.class).getValue();  
		} catch(Throwable t) {
			throw new RuntimeException("Unable to un marshal " + source + ": " + t.getMessage(), t);
		}
	}
	
	static public CometMappingData fromDocument(Document document) {
		return JAXBDeSerializationUtils.fromDocument(document, MINIMAL_JAXB_CONTEXT);
	}
	
	static public CometMappingData fromDocument(Document document, JAXBContext context) {
		try {
			return context.createUnmarshaller().unmarshal(document, CometMappingData.class).getValue();  
		} catch(Throwable t) {
			throw new RuntimeException("Unable to un marshal " + document + ": " + t.getMessage(), t);
		}
	}
	
	static public String toXML(CometMappingData mappingData) {
		return JAXBDeSerializationUtils.toXML(mappingData, MINIMAL_JAXB_CONTEXT);
	}
	
	static public String toXML(CometMappingData mappingData, JAXBContext context) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		try {
			context.createMarshaller().marshal(mappingData, baos);
			baos.flush();
			
			return new String(baos.toByteArray(), "UTF-8");
		} catch(Throwable t) {
			if(baos != null)
				try {
					baos.close();
				} catch(IOException IOe) {
					throw new RuntimeException("Unable to close byte stream: " + IOe.getMessage(), IOe);
				}
			
			throw new RuntimeException("Unable to marshal " + mappingData + ": " + t.getMessage(), t);
		}
	}
	
	static public CometMappingData fromXML(String xml) {
		return JAXBDeSerializationUtils.fromXML(xml, MINIMAL_JAXB_CONTEXT);
	}
	
	static public CometMappingData fromXML(String xml, JAXBContext context) {
		StringReader sr = new StringReader(xml);
		
		try {
			return (CometMappingData)context.createUnmarshaller().unmarshal(sr);
		} catch(Throwable t) {
			if(sr != null)
				sr.close();
			
			throw new RuntimeException("Unable to unmarshal " + xml + ": " + t.getMessage(), t);
		}
	}
}
