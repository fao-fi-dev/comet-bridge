/**
 * (c) 2014 FAO / UN (project: comet-mapping-format)
 */
package org.fao.fi.comet.bridge.mapping.dsl;

import java.net.URI;

import org.fao.fi.comet.bridge.mapping.model.CometDataProvider;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Apr 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Apr 2014
 */
public class DataProviderDSL {
	final static public CometDataProvider provider(URI identifier) {
		CometDataProvider provider = new CometDataProvider();
		provider.setProviderId(identifier);
		return provider;
	}
	
	final static public CometDataProvider provider(String identifierURI) {
		return provider(URI.create(identifierURI));
	}
	
	final static public CometDataProvider provider(String identifierURI, String dataSourceIdentifierURI) {
		return DataProviderDSL.provider(identifierURI, dataSourceIdentifierURI, null);
	}
	
	final static public CometDataProvider provider(String identifierURI, String dataSourceIdentifierURI, String version) {
		return DataProviderDSL.provider(URI.create(identifierURI), (URI)null, URI.create(dataSourceIdentifierURI), version);
	}
	
	final static public CometDataProvider provider(String identifierURI, String providedTypeURI, String dataSourceIdentifierURI, String version) {
		return new CometDataProvider(URI.create(identifierURI), URI.create(dataSourceIdentifierURI), version, URI.create(providedTypeURI));
	}
	
	final static public CometDataProvider provider(URI identifier, URI dataSourceIdentifier, String version) {
		return new CometDataProvider(identifier, dataSourceIdentifier, version, null);
	}
	
	final static public CometDataProvider provider(URI identifier, URI providedType, URI dataSourceIdentifier, String version) {
		return new CometDataProvider(identifier, dataSourceIdentifier, version, providedType);
	}
	
	final static public CometDataProvider provider(URI identifier, URI providedType, URI dataSourceIdentifier) {
		return new CometDataProvider(identifier, dataSourceIdentifier, null, providedType);
	}
}
