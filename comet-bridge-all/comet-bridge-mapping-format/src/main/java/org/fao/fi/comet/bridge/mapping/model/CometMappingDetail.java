/**
 * (c) 2014 FAO / UN (project: comet-core-vr-model)
 */
package org.fao.fi.comet.bridge.mapping.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 Apr 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 Apr 2014
 */
@XmlType(name="CometMappingDetail")
@XmlAccessorType(XmlAccessType.FIELD)
public class CometMappingDetail implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -1479077411818857917L;

	@XmlAttribute(name="score") private double _score;
	@XmlAttribute(name="scoreType") private CometMappingScoreType _scoreType;
	
	@XmlElement(name="CometMappingContribution")
	private Collection<CometMappingContribution> _mappingContributions;
	
	@XmlElement(name="TargetElement")
	private CometMappingElement _targetElement;

	/**
	 * Class constructor
	 *
	 */
	public CometMappingDetail() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param score
	 * @param scoreType
	 * @param mappingContributions
	 * @param targetElement
	 */
	public CometMappingDetail(double score, CometMappingScoreType scoreType, Collection<CometMappingContribution> mappingContributions, CometMappingElement targetElement) {
		super();
		this._score = score;
		this._scoreType = scoreType;
		this._mappingContributions = mappingContributions;
		this._targetElement = targetElement;
	}

	/**
	 * @return the 'score' value
	 */
	public final double getScore() {
		return this._score;
	}

	/**
	 * @param score the 'score' value to set
	 */
	public final void setScore(double score) {
		this._score = score;
	}
	
	/**
	 * @return the 'scoreType' value
	 */
	public CometMappingScoreType getScoreType() {
		return this._scoreType;
	}

	/**
	 * @param scoreType the 'scoreType' value to set
	 */
	public void setScoreType(CometMappingScoreType scoreType) {
		this._scoreType = scoreType;
	}

	/**
	 * @return the 'mappingContributions' value
	 */
	public Collection<CometMappingContribution> getMappingContributions() {
		return this._mappingContributions;
	}

	/**
	 * @param mappingContributions the 'mappingContributions' value to set
	 */
	public void setMappingContributions(Collection<CometMappingContribution> mappingContributions) {
		this._mappingContributions = mappingContributions;
	}
	
	public CometMappingDetail withMappingScore(double score, CometMappingScoreType type) {
		this._score = score;
		this._scoreType = type;
		
		return this;
	}
	
	public CometMappingDetail withWeightedScore(double score) {
		return this.withMappingScore(score, CometMappingScoreType.NON_AUTHORITATIVE);
	}
	
	public CometMappingDetail asContributedBy(CometMappingContribution... mappingContributions) {
		this._mappingContributions = new ArrayList<CometMappingContribution>(Arrays.asList(mappingContributions));
		
		return this;
	}
	
	public CometMappingDetail and(CometMappingContribution... mappingContributions) {
		if(this._mappingContributions == null)
			this.asContributedBy(mappingContributions);
		else for(CometMappingContribution in : mappingContributions)
			this._mappingContributions.add(in);
		
		return this;
	}

	/**
	 * @return the 'targetElement' value
	 */
	public final CometMappingElement getTargetElement() {
		return this._targetElement;
	}

	/**
	 * @param targetElement the 'targetElement' value to set
	 */
	public final void setTargetElement(CometMappingElement targetElement) {
		this._targetElement = targetElement;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._mappingContributions == null) ? 0 : this._mappingContributions.hashCode());
		long temp;
		temp = Double.doubleToLongBits(this._score);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((this._scoreType == null) ? 0 : this._scoreType.hashCode());
		result = prime * result + ((this._targetElement == null) ? 0 : this._targetElement.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;		
		CometMappingDetail other = (CometMappingDetail) obj;
		if (this._mappingContributions == null) {
			if (other._mappingContributions != null)
				return false;
		} else if (!this._mappingContributions.equals(other._mappingContributions))
			return false;
		if (Double.doubleToLongBits(this._score) != Double.doubleToLongBits(other._score))
			return false;
		if (this._scoreType != other._scoreType)
			return false;
		if (this._targetElement == null) {
			if (other._targetElement != null)
				return false;
		} else if (!this._targetElement.equals(other._targetElement))
			return false;
		return true;
	}
}
