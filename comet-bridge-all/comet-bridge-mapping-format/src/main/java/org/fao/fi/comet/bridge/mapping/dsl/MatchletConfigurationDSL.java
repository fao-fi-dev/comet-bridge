/**
 * (c) 2014 FAO / UN (project: comet-mapping-format)
 */
package org.fao.fi.comet.bridge.mapping.dsl;

import java.net.URI;

import org.fao.fi.comet.bridge.mapping.ScoreValue;
import org.fao.fi.comet.bridge.mapping.model.CometMatchletConfiguration;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Apr 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Apr 2014
 */
public class MatchletConfigurationDSL {
	final static public CometMatchletConfiguration optional(CometMatchletConfiguration configuration) {
		configuration.setOptional(true);
		
		return configuration;
	}
	
	final static public CometMatchletConfiguration configuredMatchlet(URI id) {
		return new CometMatchletConfiguration(id);
	}
	
	final static public CometMatchletConfiguration configuredMatchlet(String idURI) {
		return MatchletConfigurationDSL.configuredMatchlet(URI.create(idURI));
	}
	
	final static public double weight(double weight) {
		if(Double.compare(weight, 0) <= 0)
			throw new IllegalArgumentException("Matchlet weights must be greater than zero");
		
		return weight;
	}
	
	final static public double minimumMatchletScore(double minimumScore) {
		if(Double.compare(minimumScore, ScoreValue.NO_MATCH) < 0 || Double.compare(minimumScore, ScoreValue.FULL_MATCH) > 0) 
			throw new IllegalArgumentException("Matchlet minimum scores must be in the range [" + ScoreValue.NO_MATCH + ", " + ScoreValue.FULL_MATCH + "]");
		
		return minimumScore;
	}
}
