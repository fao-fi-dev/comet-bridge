/**
 * (c) 2014 FAO / UN (project: comet-mapping-format)
 */
package org.fao.fi.comet.bridge.mapping.dsl;

import java.net.URI;

import org.fao.fi.comet.bridge.mapping.model.CometMappingElementIdentifier;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Apr 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Apr 2014
 */
public class MappingElementIdentifierDSL {
	final static public CometMappingElementIdentifier identifier(CometMappingElementIdentifier elementId) {
		return new CometMappingElementIdentifier(elementId.getElementId());
	}

	final static public CometMappingElementIdentifier identifierFor(URI elementId) {
		return new CometMappingElementIdentifier(elementId);
	}
	
	final static public CometMappingElementIdentifier identifierFor(String elementIdURI) {
		return MappingElementIdentifierDSL.identifierFor(URI.create(elementIdURI));
	}
}