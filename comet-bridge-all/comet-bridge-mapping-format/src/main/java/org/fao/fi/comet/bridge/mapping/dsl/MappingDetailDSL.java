/**
 * (c) 2014 FAO / UN (project: comet-mapping-format)
 */
package org.fao.fi.comet.bridge.mapping.dsl;

import org.fao.fi.comet.bridge.mapping.model.CometMappingDetail;
import org.fao.fi.comet.bridge.mapping.model.CometMappingElement;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Apr 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Apr 2014
 */
public class MappingDetailDSL {
	final static public CometMappingDetail target(CometMappingElement target) {
		CometMappingDetail mapping = new CometMappingDetail();
		mapping.setTargetElement(target);
		
		return mapping;
	}
}
