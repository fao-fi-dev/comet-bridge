/**
 * (c) 2014 FAO / UN (project: comet-mapping-format)
 */
package org.fao.fi.comet.bridge.mapping.dsl;

import java.net.URI;

import org.fao.fi.comet.bridge.mapping.model.CometMappingElement;
import org.fao.fi.comet.bridge.mapping.model.CometMappingElementIdentifier;
import org.fao.fi.comet.bridge.mapping.model.utils.jaxb.JAXB2DOMUtils;
import org.w3c.dom.Element;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Apr 2014  Fiorellato   Creation.
 *
 * @version 1.0
 * @since 17 Apr 2014
 */
public class MappingElementDSL {
	final static public CometMappingElement wrap(CometMappingElementIdentifier elementIdentifier, Element data) {
		return new CometMappingElement().with(elementIdentifier).wrapping(data);
	}

	final static public CometMappingElement wrap(URI elementId, Element data) {
		return new CometMappingElement().with(elementId).wrapping(data);
	}
	
	final static public CometMappingElement wrap(String elementIdURI, Element data) {
		return MappingElementDSL.wrap(URI.create(elementIdURI), data);
	}

	final static public CometMappingElement wrap(Element data) {
		return new CometMappingElement().wrapping(data);
	}
	
	final static public CometMappingElement wrap(CometMappingElementIdentifier elementIdentifier, Object data) {
		return new CometMappingElement().with(elementIdentifier).wrapping(JAXB2DOMUtils.asElement(data));
	}
	
	final static public CometMappingElement wrap(String elementIdURI, Object data) {
		return MappingElementDSL.wrap(URI.create(elementIdURI), data);
	}
	
	final static public CometMappingElement wrap(URI elementId, Object data) {
		return new CometMappingElement().with(elementId).wrapping(JAXB2DOMUtils.asElement(data));
	}

	final static public CometMappingElement wrap(Object data) {
		return new CometMappingElement().wrapping(JAXB2DOMUtils.asElement(data));
	}
	
	final static public CometMappingElement nil() {
		return new CometMappingElement().wrapping(null);
	}
	
	final static public CometMappingElement nil(URI elementId) {
		return new CometMappingElement().with(elementId).wrapping(null);
	}
	
	final static public CometMappingElement nil(String elementIdURI) {
		return new CometMappingElement().with(URI.create(elementIdURI)).wrapping(null);
	}
}
